#include <iostream>
#include <stdlib.h>
#include <list>
#include <memory>
#include <ctime>
#include <chrono>

using namespace std;

struct sPos {
    sPos() { x = 0; y = 0; }
    sPos(int aX, int aY) { x = aX; y = aY; }
    int x;
    int y;
};

struct sSize
{
    sSize() { width = 0; height = 0; }
    sSize(int aW, int aH) { width = aW; height = aH; }
    int width;
    int height;
};

struct sRect
{
    sRect() {}
    sRect(int x, int y, int w, int h) { pos.x = x; pos.y = y; size.width = w; size.height = h; }
    sPos pos;
    sSize size;
    bool intersects(const sRect& other) {
        return !((other.pos.x + other.size.width <= pos.x) ||
                 (other.pos.y + other.size.height <= pos.y) ||
                 (other.pos.x >= pos.x + size.width) ||
                 (other.pos.y >= pos.y + size.height));
    }
};

enum class eDirection {
    UP      =   0,
    LEFT,
    RIGHT,
    DOWN
};

struct sCar {
    sRect rect;
    eDirection dir;
    int speed;
    int carNum;
    virtual ~sCar() = default;

    virtual void move() {
        switch (dir) {
        case eDirection::UP:
            rect.pos.y += speed;
            break;
        case eDirection::DOWN:
            rect.pos.y -= speed;
            break;
        case eDirection::RIGHT:
            rect.pos.x += speed;
            break;
        case eDirection::LEFT:
            rect.pos.x -= speed;
            break;
        }
//        std::cout<<"car "<<carNum<<" moved posx="<<rect.pos.x<<" posY="<<rect.pos.y<<" dir="<<static_cast<int>(dir)<<std::endl;
    }

    sRect getFuturePos() {
        switch (dir) {
        case eDirection::UP:
            return sRect(rect.pos.x, rect.pos.y + speed, rect.size.width, rect.size.height);
        case eDirection::DOWN:
            return sRect(rect.pos.x, rect.pos.y - speed, rect.size.width, rect.size.height);
        case eDirection::RIGHT:
            return sRect(rect.pos.x + speed, rect.pos.y, rect.size.width, rect.size.width);
        case eDirection::LEFT:
            return sRect(rect.pos.x + speed, rect.pos.y, rect.size.width, rect.size.height);
        }
    }

    virtual int getFuel() = 0;
    virtual void refill(int count) = 0;
};

struct sGasEngine : virtual sCar {
    int getFuel() { return fuel; }
    void refill(int count) { fuel += count; }
    void move() { fuel--; sCar::move(); }
    int fuel;
};

struct sElectroCar : virtual sCar {
    int getFuel() { return charge; }
    void refill(int count) { charge += count; }
    void move() { charge--; sCar::move(); }
    int charge;
};

struct sHybrid : sGasEngine, sElectroCar {
    void refill(int count) { charge += count / 2; fuel += count / 2; }
    int getFuel() { return charge + fuel; }
    void move() {
        if (rand() % 2 == 0)
            charge--;
        else
            fuel--;
        sCar::move();
    }
};

//std::vector<std::unique_ptr<sCar>> asdasd;
std::list<std::unique_ptr<sCar>> passed;
std::list<std::unique_ptr<sCar>> fromLeft;
std::list<std::unique_ptr<sCar>> fromTop;
std::list<std::unique_ptr<sCar>> fromRight;
std::list<std::unique_ptr<sCar>> fromBot;

const int initialCarsCount = 10;

#define SCREEN_WIDTH 1024
#define SCREEN_HEIGHT 768

std::unique_ptr<sCar> spawnCarFromSide(eDirection side) {
    std::unique_ptr<sCar> car;
    int carType = rand();
    switch (carType % 3) {
    case 1:
        car = std::make_unique<sElectroCar>();
        break;
    case 2:
        car = std::make_unique<sHybrid>();
        break;
    default:
        car = std::make_unique<sGasEngine>();
    }
    car->speed = 1;
    static int cNum=1;
    car->carNum=cNum++;
    switch (side) {
    case eDirection::DOWN:
        car->rect = sRect(SCREEN_WIDTH / 2, 0, 100, 100);
        car->dir = eDirection::UP;
        break;
    case eDirection::UP:
        car->rect = sRect(SCREEN_WIDTH / 2, SCREEN_HEIGHT, 100, 100);
        car->dir = eDirection::DOWN;
        break;
    case eDirection::LEFT:
        car->rect = sRect(0, SCREEN_HEIGHT / 2, 100, 100);
        car->dir = eDirection::RIGHT;
        break;
    case eDirection::RIGHT:
        car->rect = sRect(SCREEN_WIDTH, SCREEN_HEIGHT / 2, 100, 100);
        car->dir = eDirection::LEFT;
        break;
    }
    return car;
}

void spawnCar() {
    auto rnd = rand();
    if ( rnd % 4 == 0)
        fromRight.push_back(std::move(spawnCarFromSide(eDirection::RIGHT)));
    else if (rnd % 4 == 1)
        fromTop.push_back(std::move(spawnCarFromSide(eDirection::UP)));
    else if (rnd % 4 == 2)
        fromBot.push_back(std::move(spawnCarFromSide(eDirection::DOWN)));
    else if (rnd % 4 == 3)
        fromLeft.push_back(std::move(spawnCarFromSide(eDirection::LEFT)));
}

void movePassed() {
    for(auto &car:passed){
        car->move();
    }
}

void moveFirst(){
    auto carL=fromLeft.begin();
    auto carB=fromBot.begin();
    auto carR=fromRight.begin();
    auto carT=fromTop.begin();
    bool moved=false;
    if(carL!=fromLeft.end()){
        if(carB!=fromBot.end()){
            auto t1=(*carB)->getFuturePos();
            auto t2=(*carL)->getFuturePos();
            auto t3=t1.intersects(t2);
            if(!(*carB)->getFuturePos().intersects((*carL)->getFuturePos())){
                (*carL)->move();
                moved=true;
            }
        }else{
            (*carL)->move();
            moved=true;
        }
    }
    if(carB!=fromBot.end()){
        if(carR!=fromRight.end()){
            auto t1=(*carR)->getFuturePos();
            auto t2=(*carB)->getFuturePos();
            auto t3=t1.intersects(t2);
            if(!(*carR)->getFuturePos().intersects((*carB)->getFuturePos())){
                (*carB)->move();
                moved=true;
            }
        }else{
            (*carB)->move();
            moved=true;
        }
    }
    if(carR!=fromRight.end()){
        if(carT!=fromTop.end()){
            auto t1=(*carR)->getFuturePos();
            auto t2=(*carT)->getFuturePos();
            auto t3=t1.intersects(t2);
            if(!(*carT)->getFuturePos().intersects((*carR)->getFuturePos())){
                (*carR)->move();
                moved=true;
            }
        }else{
            (*carR)->move();
            moved=true;
        }
    }
    if(carT!=fromTop.end()){
        if(carL!=fromLeft.end()){
            auto t1=(*carL)->getFuturePos();
            auto t2=(*carT)->getFuturePos();
            auto t3=t1.intersects(t2);
            if(!(*carL)->getFuturePos().intersects((*carT)->getFuturePos())){
                (*carT)->move();
                moved=true;
            }
        }else{
            (*carT)->move();
            moved=true;
        }
    }
    //
    if(!moved) (*carL)->move();
    //
}

void moveLineLast(std::list<std::unique_ptr<sCar>>& carList){
    if(carList.size()>1){
        auto prevFuturePos=(*(carList.begin()))->rect;
        for(auto car=++carList.begin();car!=carList.end();++car){
            auto currFuturePos=(*car)->getFuturePos();
            if(!currFuturePos.intersects(prevFuturePos)){
                (*car)->move();
            }
            prevFuturePos=currFuturePos;
        }
    }
}

void moveLast(){
    moveLineLast(fromLeft);
    moveLineLast(fromTop);
    moveLineLast(fromBot);
    moveLineLast(fromRight);
}

void checkLists(){
    for(auto it=passed.begin();it!=passed.end();){
        if ((*it)->rect.pos.x < 0 || (*it)->rect.pos.x > SCREEN_WIDTH || (*it)->rect.pos.y < 0 || (*it)->rect.pos.y > SCREEN_HEIGHT){
            cout<<"car delete car="<<(*it)->carNum<<endl;
            it=passed.erase(it);
            spawnCar();
        }else{
            ++it;
        }
    }
    auto car=fromLeft.begin();
    if(car!=fromLeft.end()){
        auto t1=(*car)->rect.pos.x;
        auto t2=(*car)->rect.size.width;
        if((*car)->rect.pos.x+(*car)->rect.size.width>SCREEN_WIDTH/2+50){
//            cout<<"car move to path car="<<(*car)->carNum<<endl;
            passed.push_back(std::move((*car)));
            fromLeft.pop_front();
        }
    }
    car=fromRight.begin();
    if(car!=fromRight.end()){
        auto t1=(*car)->rect.pos.x;
        auto t2=(*car)->rect.size.width;
        if((*car)->rect.pos.x+(*car)->rect.size.width<(SCREEN_WIDTH/2-50)){
//            cout<<"car move to path car="<<(*car)->carNum<<endl;
            passed.push_back(std::move((*car)));
            fromRight.pop_front();
        }
    }
    car=fromTop.begin();
    if(car!=fromTop.end()){
        auto t1=(*car)->rect.pos.x;
        auto t2=(*car)->rect.size.width;
        if((*car)->rect.pos.y+(*car)->rect.size.height<SCREEN_HEIGHT/2-50){
//            cout<<"car move to path car="<<(*car)->carNum<<endl;
            passed.push_back(std::move((*car)));
            fromTop.pop_front();
        }
    }
    car=fromBot.begin();
    if(car!=fromBot.end()){
        auto t1=(*car)->rect.pos.x;
        auto t2=(*car)->rect.size.width;
        if((*car)->rect.pos.y+(*car)->rect.size.height>SCREEN_HEIGHT/2+50){
//            cout<<"car move to path car="<<(*car)->carNum<<endl;
            passed.push_back(std::move((*car)));
            fromBot.pop_front();
        }
    }
}

void main_loop() {
    std::clock_t c_start = std::clock();
    while(true){
        //
//        auto t_start=std::chrono::high_resolution_clock::now();
        //
        movePassed();
        //
        moveFirst();
        //
        moveLast();
        //
        checkLists();
        //
//        auto t_end=std::chrono::high_resolution_clock::now();
//        cout<<"time = "<<std::chrono::duration<double, std::milli>(t_end-t_start).count()<<endl;
        //
    }
}

int main(int argc, char** argv) {
    for (auto i = 0; i < initialCarsCount; ++i) {
        spawnCar();
    }
    main_loop();
    return 0;
}
